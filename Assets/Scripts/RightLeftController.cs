﻿using UnityEngine;
using System.Collections;

public class RightLeftController : MonoBehaviour {
	private RightLeftController bounceObject; //for caching this transform
	/*
	public float MoveSpeed = 0.5f;
	// Use this for initialization
	void Start () {
		float h = Input.GetAxis("Horizontal") * MoveSpeed;
		bounceObject = this;
		bounceObject.transform.Translate(h*Time.deltaTime,0,0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	*/

	public float speed = 1f ;
	public float distance =5f;
	public bool leftoright = true;
	private float xStartPosition;
	void Start () {
		xStartPosition = transform.position.x;
		bounceObject = this;
	}
	void Update () {
		if (leftoright){
			if ((speed < 0 && bounceObject.transform.position.x < xStartPosition) || (speed > 0 && bounceObject.transform.position.x > xStartPosition + distance))
			{
				speed *= -1;
			}
		}
		if (!leftoright) {
						if ((speed < 0 && bounceObject.transform.position.x > xStartPosition) || (speed > 0 && bounceObject.transform.position.x < xStartPosition - distance)) {
								speed *= -1;
						}
		}



		if (leftoright)
		  bounceObject.transform.position = new Vector2(bounceObject.transform.position.x + speed * Time.deltaTime, bounceObject.transform.position.y);
		else 
		  bounceObject.transform.position = new Vector2(bounceObject.transform.position.x - speed * Time.deltaTime, bounceObject.transform.position.y);

	}

}

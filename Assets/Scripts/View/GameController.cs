﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class GameController : MonoBehaviour {
	RaycastHit hit;
	Ray ray;
	public LayerMask touchInPutMask;
	RuntimePlatform platform = Application.platform;
	public GameObject restartDialog;

	public Text txtScore;
	public Text txtScoreResult;
	public Text txtHighScore;
	int score=0;
	int game_over;
	GenerateObject generateObject ;
	List<GameObject> objects; 

	// Use this for initialization
	void Start () {
	
		//initiate 
		PlayerPrefs.SetInt("game_over",0);
		restartDialog.SetActive(false);

		GameObject gameControllerObject = GameObject.FindWithTag ("MainGame");
		
		if (gameControllerObject != null)
		{
			generateObject = gameControllerObject.GetComponent <GenerateObject>();
			//Debug.Log ("Find 'MainGame' script");
		}
		if (gameControllerObject == null)
		{
			//Debug.Log ("Cannot find 'MainGame' script");
		}
	}

	void checkTouch(Vector3 pos){
		Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
		Vector2 touchPos = new Vector2(wp.x, wp.y);
		var hit = Physics2D.OverlapPoint(touchPos);
		string left_cloud = PlayerPrefs.GetString ("left_cloud");
		string right_cloud = PlayerPrefs.GetString ("right_cloud");
		if(hit){

			objects = generateObject.GetAllObjectGame();

			//Debug.Log(hit.transform.gameObject.name);
			//hit.transform.gameObject.SendMessage('Clicked',0,SendMessageOptions.DontRequireReceiver);
			objects.Remove(hit.transform.gameObject);
			hit.transform.gameObject.SetActive(false);
			//(hit.transform.gameObject);


			//check user click to right or to left 
			if(pos.x <(Screen.width/2))
				
			{
				//Debug.Log("Left" + wp.x);

				if(hit.transform.gameObject.name.Contains(left_cloud)){
					score++;
					txtScore.text = score.ToString();

				}
				
			}else{

				//Debug.Log("Right" + wp.x);
				
				if(hit.transform.gameObject.name.Contains(right_cloud)){
					score++;
					txtScore.text = score.ToString();
					
				}
			}

			txtScoreResult.text = score.ToString();







		}
	}

	public void RestartGame(){
		restartDialog.SetActive (false);
		score = 0;
		PlayerPrefs.SetInt("game_over",0);

	}

	// Update is called once per frame
	void Update () {
	
		//Debug.Log (platform);
		if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer){
			if(Input.touchCount > 0) {
				if(Input.GetTouch(0).phase == TouchPhase.Began){
					checkTouch(Input.GetTouch(0).position);
				}
			}
		}else if(platform == RuntimePlatform.WindowsEditor || platform ==RuntimePlatform.OSXEditor){
			if(Input.GetMouseButtonDown(0)) {
				checkTouch(Input.mousePosition);
			}
		}

		game_over = PlayerPrefs.GetInt("game_over");

		if(game_over==1)
		{
			restartDialog.SetActive (true);

			objects = generateObject.GetAllObjectGame();
			foreach (var obj in objects){
				if(obj!=null)
					obj.tag = "collid_disable";
			}

		

		}

	}


 

}

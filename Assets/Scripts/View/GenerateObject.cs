﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateObject : MonoBehaviour {

	public GameObject rightCloud;
	public GameObject leftCloud;
	public GameObject[] availableObjects;    
	public List<GameObject> objects;


	public GameObject toprightLineColor; 
	public GameObject bottomrightLineColor;


	public GameObject topleftLineColor;
	public GameObject bottomleftLineColor;
	public GameObject groundbeyon; 

	int game_over;

	//private Vector3 aPosition = new Vector3(0f,0f, 0);
	// Use this for initialization
	void Start () {

		//addCloud (1, aPosition);
		InvokeRepeating ("changColorCloud", 3, 6);
		//.Log ("Width:" + Screen.width);
		InvokeRepeating ("addFourDropWater", 1, 1);


		//Vector3 position1 = new Vector3(0f,8f, 0);
		//Vector3 position2 = new Vector3(0f,-24f, 0);
		//Debug.DrawLine(position1, position2, Color.green, 2, false);

		//Debug.Log ("ground bound" + groundbeyon.transform.position.x + "," + groundbeyon.transform.position.y);
	}
	
	// Update is called once per frame
	void Update () {
	

	}





	void changColorCloud()
	{
		int randomIndex = Random.Range(0, 5);
		int randomeNext = Random.Range (0, 1);
		string strleftCloud = "Sprites/cloud/red_cloud";
		string strrightCloud = "Sprites/cloud/blue_cloud";

		string strLeftColor = "Sprites/line/red_line";
	

		string strRightColor = "Sprites/line/blue_line";
	
		switch(randomIndex){
		case 0:
			strleftCloud = "Sprites/cloud/red_cloud";
			strrightCloud = "Sprites/cloud/blue_cloud";

			PlayerPrefs.SetString("left_cloud", "red");
			PlayerPrefs.SetString("right_cloud", "blue");

			strLeftColor = "Sprites/line/red_line";
			strRightColor = "Sprites/line/blue_line";



			break;
		case 1:
			strleftCloud = "Sprites/cloud/blue_cloud";
			strrightCloud = "Sprites/cloud/red_cloud";

			PlayerPrefs.SetString("left_cloud", "blue");
			PlayerPrefs.SetString("right_cloud", "red");


			strLeftColor = "Sprites/line/blue_line";
			strRightColor = "Sprites/line/red_line";

			break;
		case 2:
			strleftCloud = "Sprites/cloud/green_cloud";
			strrightCloud = "Sprites/cloud/blue_cloud";

			PlayerPrefs.SetString("left_cloud", "green");
			PlayerPrefs.SetString("right_cloud", "blue");

			strLeftColor = "Sprites/line/green_line";			
			strRightColor = "Sprites/line/blue_line";

			break;
		case 3:
			strleftCloud = "Sprites/cloud/yellow_cloud";
			strrightCloud = "Sprites/cloud/red_cloud";

			PlayerPrefs.SetString("left_cloud", "yellow");
			PlayerPrefs.SetString("right_cloud", "red");

			strLeftColor = "Sprites/line/yellow_line";
			strRightColor = "Sprites/line/red_line";
		

			break;
		case 4:
			strleftCloud ="Sprites/cloud/blue_little_cloud";
			strrightCloud = "Sprites/cloud/red_cloud";

			PlayerPrefs.SetString("left_cloud", "blue_little");
			PlayerPrefs.SetString("right_cloud", "red");

			strLeftColor = "Sprites/line/blue_little_line";
			strRightColor = "Sprites/line/red_line";

			break;
		default:
			strleftCloud = "Sprites/cloud/red_cloud";
			strrightCloud = "Sprites/cloud/green_cloud";

			PlayerPrefs.SetString("left_cloud", "red");
			PlayerPrefs.SetString("right_cloud", "green");

			strLeftColor = "Sprites/line/red_line";
			strRightColor = "Sprites/line/green_line";

			break;
		}


		//All of objects ( drops)  in under bounder will change tag to nothing, so we will not need to check these
		if(game_over==0){
			objects = GetAllObjectGame();
			foreach (var obj in objects){
				if(obj!=null){
					if(obj.transform.position.y < groundbeyon.transform.position.y){
						
						obj.tag = "collid_disable";
						Debug.Log("Collid disable for " +obj.name);
					}
				}
			}
		}

		//Debug.Log (randomIndex);
		if (randomeNext == 0) 
		{
			string strLeft = strleftCloud;
			strleftCloud = strrightCloud;
			strrightCloud = strLeft;


		}

		Sprite spriteLeft = Resources.Load(strleftCloud, typeof(Sprite)) as Sprite;
		leftCloud.GetComponent<SpriteRenderer>().sprite = spriteLeft;

		Sprite sprite = Resources.Load(strrightCloud, typeof(Sprite)) as Sprite;
		rightCloud.GetComponent<SpriteRenderer>().sprite = sprite;


		Sprite spriteTopLeftColor = Resources.Load(strLeftColor, typeof(Sprite)) as Sprite;
		topleftLineColor.GetComponent<SpriteRenderer>().sprite = spriteTopLeftColor;


		Sprite spriteTopRightColor = Resources.Load(strRightColor, typeof(Sprite)) as Sprite;
		toprightLineColor.GetComponent<SpriteRenderer>().sprite = spriteTopRightColor;

		Sprite spriteBottomLeftColor = Resources.Load(strLeftColor, typeof(Sprite)) as Sprite;
		bottomleftLineColor.GetComponent<SpriteRenderer>().sprite = spriteBottomLeftColor;

		Sprite spriteBottomRightColor = Resources.Load(strRightColor, typeof(Sprite)) as Sprite;
		bottomrightLineColor.GetComponent<SpriteRenderer>().sprite = spriteBottomRightColor;


	}

	void addFourDropWater()
   {
		game_over = PlayerPrefs.GetInt("game_over");
		if(game_over==0){

			int randomLeft = Random.Range(0, 2);
			int randomeRight = Random.Range (0, 2);

			Vector3 position1 = new Vector3(-4f,12f, 0);
			Vector3 position2 = new Vector3(-1.8f,12f, 0);
			Vector3 position3 = new Vector3(1.8f,12f, 0);
			Vector3 position4 = new Vector3(4f,12f, 0);
			if(randomLeft==0)
				addADropWater (1, position1);
			else
				addADropWater (1, position2);

			if(randomeRight==0)
				addADropWater (1, position3);
			else
				addADropWater (1, position4);

		}
	
	}

	void addADropWater(int type, Vector3 pos)
	{
		int randomIndex = Random.Range(0, availableObjects.Length);
		//GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);
		
		GameObject g = (GameObject)Instantiate(availableObjects[randomIndex]);
		g.transform.parent = this.transform;
		g.transform.position = pos;
		g.tag = "collid_enable";
		objects.Add (g);
	}

	void addCloud(int type,Vector3 pos)
	{
		int randomIndex = Random.Range(0, availableObjects.Length);
		//GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);

		GameObject g = (GameObject)Instantiate(availableObjects[randomIndex]);
		g.transform.parent = this.transform;
		g.transform.position = pos;
		//change sprite it works 
		Sprite sprite = Resources.Load("Sprites/cloud/red_cloud", typeof(Sprite)) as Sprite;
		g.GetComponent<SpriteRenderer>().sprite = sprite;


		//g.GetComponent<BoxCollider> ().isTrigger = true;
		//GameObject g = GameObject.CreatePrimitive(PrimitiveType.Cube);
		//BoxCollider _bc = (BoxCollider)g.AddComponent(typeof(BoxCollider));
		//_bc.center = Vector3.zero;
		//g.GetComponent<BoxCollider> ().isTrigger = true; 
		//_bc.isTrigger = true;

		//BoxCollider _bc = (BoxCollider)level001cube001.gameObject.AddComponent(typeof(BoxCollider));
		//g.transform.localScale = new Vector3(Mathf.Abs(width * 0.95f / sprite.bounds.size.x), Mathf.Abs (- height * 0.95f / sprite.bounds.size.y), 1);
		//g.tag = "item1";
		objects.Add(g);

	   
	}


	public List<GameObject> GetAllObjectGame()
	{
		return objects;
	}







}
